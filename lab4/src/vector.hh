
#include <iostream>
#include <stdlib.h>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <utility>

template<typename T>
class Vector {
private:
    T *vals;
    size_t cap;
    size_t len;

    void copy_helper(const Vector<T> &vec) {
        this->vals = (T*) malloc(vec.capacity() * sizeof (T));
        std::memcpy(this->vals, vec.data(), vec.size() * sizeof (T));
        this->cap = vec.capacity();
        this->len = vec.size();
    }

    void move_helper(Vector<T> &&vec) {
        if (this != &vec) {
            this->cap = vec.capacity();
            this->len = vec.size();
            if (this->vals) {
                delete[] this->vals;
                this->vals = nullptr;
            }
            this->vals = vec.data();
            vec.vals = nullptr;
        }
    }

    void cleanup_helper() {
        if (this->vals) {
            delete[] this->vals;
            this->vals = nullptr;
        }
        this->cap = 0;
        this->len = 0;
    }


public:
    typedef T* iterator;

    // CONSTRUCTOR
    Vector(size_t size = 0, size_t capacity = 0)
        : vals((T*) malloc((capacity == 0 ? size : capacity) * sizeof (T)))
        , cap(capacity == 0 ? size : capacity)
        , len(size)
    {
        assert(capacity == 0 || capacity >= size);
        //
    }

    Vector(const Vector<T> &vec) : vals(nullptr), cap(0), len(0) {
        copy_helper(vec);
    }

    Vector(Vector<T> &&vec) : vals(nullptr), cap(0), len(0) {
        move_helper(std::forward< Vector<T> >(vec));
    }

    ~Vector() {
        cleanup_helper();
    }


    // ACCESSORS
    T *data() {
        return this->vals;
    }

    const T *data() const {
        return const_cast<const T*>(this->vals);
    }

    size_t capacity() {
        return this->cap;
    }

    const size_t capacity() const {
        return this->cap;
    }

    size_t size() {
        return this->len;
    }

    const size_t size() const {
        return this->len;
    }

    T &at(size_t index) {
        assert(index < size());
        return (*this)[index];
    }

    const T &at(size_t index) const {
        assert(index < size());
        return (*this)[index];
    }

    iterator begin() {
        return this->vals;
    }

    iterator end() {
        return this->vals + this->len;
    }


    // MODIFIERS
    void resize(size_t size) {
        reserve(size);

        if (size > this->len) {
            for (size_t i = this->len; i < size; i++) {
                this->vals[i] = T();
            }
        }

        this->len = size;
    }

    void clear() {
        resize(0);
    }

    void reserve(size_t new_cap) {
        if (new_cap > this->cap) {
            this->vals = (T*) realloc(this->vals, new_cap * sizeof (T));
            this->cap = new_cap;
        }
    }

    void shrink_to_fit() {
        assert(this->len <= this->cap);
        if (this->len < this->cap) {
            this->vals = (T*) realloc(this->vals, this->len * sizeof (T));
            this->cap = this->len;
        }
    }

    void push_back(const T &val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = val;
    }

    void push_back(T &&val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = std::forward<T>(val);
    }

    void insert(iterator pos, const T &val) {
        // printf("COPY INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len++);
        memmove(pos + 1, pos, (end() - pos) * sizeof (T));
        *pos = val;
    }

    void insert(iterator pos, T &&val) {
        // printf("MOVE INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len++);
        memmove(pos + 1, pos, (end() - pos) * sizeof (T));
        *pos = std::forward<T>(val);
    }

    void erase(iterator first, iterator last) {
        assert(first <= last);
        assert(first >= begin() && first <= end());
        assert(last >= begin() && last <= end());
        memmove(first, last, (end() - last) * sizeof (T));
        this->len -= last - first;
    }

    void erase(iterator pos) {
        erase(pos, pos + 1);
    }


    // OPERATORS
    T &operator[](size_t index) {
        return this->vals[index];
    }

    const T &operator[](size_t index) const {
        return this->vals[index];
    }

    Vector &operator=(const Vector<T> &vec) {
        cleanup_helper();
        copy_helper(vec);
        return *this;
    }

    Vector &operator=(Vector<T> &&vec) {
        cleanup_helper();
        move_helper(std::forward< Vector<T> >(vec));
        return *this;
    }
};


template<>
class Vector<void*> {
private:
    void **vals;
    size_t cap;
    size_t len;

    void copy_helper(const Vector<void*> &vec) {
        this->vals = (void**) malloc(vec.capacity() * sizeof (void*));
        std::memcpy(this->vals, vec.data(), vec.size() * sizeof (void*));
        this->cap = vec.capacity();
        this->len = vec.size();
    }

    void move_helper(Vector<void*> &&vec) {
        if (this != &vec) {
            this->cap = vec.capacity();
            this->len = vec.size();
            if (this->vals) {
                delete[] this->vals;
                this->vals = nullptr;
            }
            this->vals = vec.data();
            vec.vals = nullptr;
        }
    }

    void cleanup_helper() {
        if (this->vals) {
            delete[] this->vals;
            this->vals = nullptr;
        }
        this->cap = 0;
        this->len = 0;
    }


public:
    typedef void** iterator;

    // CONSTRUCTOR (private to disable direct construction of Vector)
    Vector(size_t size = 0, size_t capacity = 0)
        : vals((void**) malloc((capacity == 0 ? size : capacity)
            * sizeof (void*)))
        , cap(capacity == 0 ? size : capacity)
        , len(size)
    {
        assert(capacity == 0 || capacity >= size);
        //
    }

    Vector(const Vector<void*> &vec) : vals(nullptr), cap(0), len(0) {
        copy_helper(vec);
    }

    Vector(Vector<void*> &&vec) : vals(nullptr), cap(0), len(0) {
        move_helper(std::forward< Vector<void*> >(vec));
    }

    ~Vector() {
        cleanup_helper();
    }


    // ACCESSORS
    void **data() {
        return this->vals;
    }

    const void **data() const {
        return const_cast<const void**>(this->vals);
    }

    size_t capacity() {
        return this->cap;
    }

    const size_t capacity() const {
        return this->cap;
    }

    size_t size() {
        return this->len;
    }

    const size_t size() const {
        return this->len;
    }

    iterator begin() {
        return this->vals;
    }

    iterator end() {
        return this->vals + this->len;
    }

    void* &at(size_t index) {
        assert(index < size());
        return (*this)[index];
    }

    const void* &at(size_t index) const {
        assert(index < size());
        return (*this)[index];
    }


    // MODIFIERS
    void resize(size_t size) {
        reserve(size);

        if (size > this->len) {
            for (size_t i = this->len; i < size; i++) {
                this->vals[i] = nullptr;
            }
        }

        this->len = size;
    }

    void clear() {
        resize(0);
    }

    void reserve(size_t new_cap) {
        if (new_cap > this->cap) {
            this->vals = (void**) realloc(this->vals, new_cap * sizeof (void*));
            this->cap = new_cap;
        }
    }

    void shrink_to_fit() {
        assert(this->len <= this->cap);
        if (this->len < this->cap) {
            this->vals =
                (void**) realloc(this->vals, this->len * sizeof (void*));
            this->cap = this->len;
        }
    }

    void push_back(const void* &val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = const_cast<void*>(val);
    }

    void push_back(void* &&val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = std::forward<void*>(val);
    }

    void insert(iterator pos, const void* &val) {
        // printf("COPY INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len++);
        memmove(pos + 1, pos, (end() - pos) * sizeof (void*));
        *pos = const_cast<void*>(val);
    }

    void insert(iterator pos, void* &&val) {
        // printf("MOVE INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len++);
        memmove(pos + 1, pos, (end() - pos) * sizeof (void*));
        *pos = std::forward<void*>(val);
    }

    void erase(iterator first, iterator last) {
        assert(first <= last);
        assert(first >= begin() && first <= end());
        assert(last >= begin() && last <= end());
        memmove(first, last, (end() - last) * sizeof (void*));
        this->len -= last - first;
    }

    void erase(iterator pos) {
        erase(pos, pos + 1);
    }


    // OPERATORS
    void* &operator[](size_t index) {
        return this->vals[index];
    }

    const void* &operator[](size_t index) const {
        return const_cast<const void**>(this->vals)[index];
    }

    Vector &operator=(const Vector<void*> &vec) {
        cleanup_helper();
        copy_helper(vec);
        return *this;
    }

    Vector &operator=(Vector<void*> &&vec) {
        cleanup_helper();
        move_helper(std::forward< Vector<void*> >(vec));
        return *this;
    }
};

template<typename T>
class Vector<T*> : private Vector<void*> {
private:
    typedef Vector<void*> Base;

public:
    typedef T** iterator;

    // CONSTRUCTOR
    Vector(size_t size = 0, size_t capacity = 0) : Base(size, capacity) {}
    Vector(const Vector<T*> &vec)
        : Base(reinterpret_cast<const Base&>(vec)) {}
    Vector(Vector<T*> &&vec)
        : Base(std::forward<Base>(reinterpret_cast<Base&&>(vec))) {}
    ~Vector() {}

    // ACCESSORS
    T **data() { return reinterpret_cast<T**>(Base::data()); }
    const T **data() const { return reinterpret_cast<const T**>(Base::data()); }
    size_t capacity() { return Base::capacity(); }
    const size_t capacity() const { return Base::capacity(); }
    size_t size() { return Base::size(); }
    const size_t size() const { return Base::size(); }
    iterator begin() { return reinterpret_cast<iterator>(Base::begin()); }
    iterator end() { return reinterpret_cast<iterator>(Base::end()); }
    T* &at(size_t index) { return reinterpret_cast<T* &>(Base::at(index)); }
    const T* &at(size_t index) const {
        return reinterpret_cast<const T* &>(Base::at(index));
    }

    // MODIFIERS
    void resize(size_t size) { Base::resize(size); }
    void clear() { Base::clear(); }
    void reserve(size_t new_cap) { Base::reserve(new_cap); }
    void shrink_to_fit() { Base::shrink_to_fit(); }
    void push_back(const T* &val) {
        Base::push_back(reinterpret_cast<const void* &>(val));
    }
    void push_back(T* &&val) {
        Base::push_back(std::forward<void*>(reinterpret_cast<void* &&>(val)));
    }
    void insert(iterator pos, const T* &val) {
        Base::insert(reinterpret_cast<Base::iterator>(pos),
            reinterpret_cast<const void* &>(val));
    }
    void insert(iterator pos, T* &&val) {
        Base::insert(reinterpret_cast<Base::iterator>(pos),
            std::forward<void*>(reinterpret_cast<void* &&>(val)));
    }
    void erase(iterator first, iterator last) {
        Base::erase(reinterpret_cast<Base::iterator>(first),
            reinterpret_cast<Base::iterator>(last));
    }
    void erase(iterator pos) {
        Base::erase(reinterpret_cast<Base::iterator>(pos));
    }

    // OPERATORS
    T* &operator[](size_t index) {
        return reinterpret_cast<T* &>(Base::operator[](index));
    }
    const T* &operator[](size_t index) const {
        return reinterpret_cast<const T* &>(Base::operator[](index));
    }
    Vector &operator=(const Vector<T*> &vec) {
        return reinterpret_cast< Vector<T*>& >(
            Base::operator=(reinterpret_cast<const Base&>(vec)));
    }
    Vector &operator=(Vector<T*> &&vec) {
        return reinterpret_cast< Vector<T*>& >(
            Base::operator=(std::forward<Base>(reinterpret_cast<Base&&>(vec))));
    }
};


template<>
class Vector<bool> {
private:
    typedef uint32_t BitPage;
    static const size_t k_bitpage_size = 8 * sizeof (BitPage);

    static uint32_t _bit_mask(size_t bit_id) {
        assert(bit_id < k_bitpage_size);
        return 1 << (k_bitpage_size - bit_id - 1);
    }

    static uint32_t _bit_mask(size_t bit_id_begin, size_t bit_id_end) {
        assert(bit_id_begin >= 0 && bit_id_begin <= k_bitpage_size);
        assert(bit_id_end >= 0 && bit_id_end <= k_bitpage_size);
        return (bit_id_begin == 0 ? 0 : _bit_mask(bit_id_begin - 1))
            - (bit_id_end == 0 ? 0 : _bit_mask(bit_id_end - 1));
    }

    static size_t _index_to_page_id(size_t index) {
        return index / k_bitpage_size;
    }

    static size_t _index_to_bit_id(size_t index) {
        return index % k_bitpage_size;
    }

    static size_t _required_page_count(size_t size) {
        return size / k_bitpage_size + ((size % k_bitpage_size) == 0 ? 0 : 1);
    }

    static size_t _page_bit_id_to_index(size_t page_id, size_t bit_id) {
        return page_id * k_bitpage_size + bit_id;
    }

    size_t _last_page_id() {
        size_t last_page_id = _required_page_count(this->len);
        if (this->len != 0) {
            last_page_id--;
        }
        return last_page_id;
    }

    Vector<BitPage> pages;

    size_t cap;
    size_t len;

public:
    class _write_helper {
    private:
        Vector<bool> *vec;
        size_t page_id;
        size_t bit_id;

        _write_helper();   // disable default construction

    public:
        _write_helper(Vector<bool> *vec, size_t index)
            : vec(vec)
            , page_id(_index_to_page_id(index))
            , bit_id(_index_to_bit_id(index))
        {
            assert(vec->size() > index);
        }

        _write_helper(Vector<bool> *vec, size_t page_id, size_t bit_id)
            : vec(vec)
            , page_id(page_id)
            , bit_id(bit_id)
        {
            assert(vec->pages.size() > page_id && bit_id < k_bitpage_size);
        }

        ~_write_helper() {}

        operator bool() const {
            size_t curr_index =
                _page_bit_id_to_index(this->page_id, this->bit_id);
            assert(this->vec->capacity() > curr_index);
            return this->vec->pages[this->page_id] & _bit_mask(this->bit_id);
        }

        bool operator=(const bool val) {
            if (val) {
                this->vec->pages.at(this->page_id) |= _bit_mask(this->bit_id);
            } else {
                this->vec->pages.at(this->page_id) &= ~_bit_mask(this->bit_id);
            }
            return val;
        }

        bool operator=(const _write_helper &rhs) {
            bool val = (bool) rhs;
            if (val) {
                this->vec->pages.at(this->page_id) |= _bit_mask(this->bit_id);
            } else {
                this->vec->pages.at(this->page_id) &= ~_bit_mask(this->bit_id);
            }
            return val;
        }
    };

    class iterator {
    private:
        friend class Vector<bool>;

        Vector<bool> *vec;
        int page_id;    // Let page_id to go negative for flexibility.
        size_t bit_id;

        iterator(); // disable default construction

        static int _index_to_page_id(int index) {
            return index / k_bitpage_size +
                (index < 0 && index % k_bitpage_size != 0 ? -1 : 0);
        }

        static size_t _index_to_bit_id(int index) {
            return (size_t) (index % k_bitpage_size +
                (index < 0 && index % k_bitpage_size != 0 ?
                    k_bitpage_size : 0));
        }

        const int _index() const {
            return this->page_id * k_bitpage_size + this->bit_id;
        }

    public:
        iterator(const Vector<bool> *vec)
            : vec(const_cast< Vector<bool>* >(vec))
            , page_id(0)
            , bit_id(0)
        {
            //
        }

        iterator(const Vector<bool> *vec, size_t index)
            : vec(const_cast< Vector<bool>* >(vec))
            , page_id(_index_to_page_id(index))
            , bit_id(_index_to_bit_id(index))
        {
            //
        }

        iterator(const Vector<bool> *vec, int index)
            : vec(const_cast< Vector<bool>* >(vec))
            , page_id(_index_to_page_id(index))
            , bit_id(_index_to_bit_id(index))
        {
            //
        }

        ~iterator() {}

        const bool operator*() const {
            assert(-1 < this->_index());
            assert((int) this->vec->size() > this->_index());
            return this->vec->pages.at(this->page_id) & _bit_mask(this->bit_id);
        }

        _write_helper operator*() {
            assert(-1 < this->_index());
            assert((int) this->vec->size() > this->_index());
            return _write_helper(this->vec, (size_t) this->page_id,
                this->bit_id);
        }


        // INC/DECREMENT OPERATORS
        iterator &operator++() {
            if (this->bit_id == k_bitpage_size - 1) {
                this->bit_id = 0;
                this->page_id++;
            } else {
                this->bit_id++;
            }
            return *this;
        }

        iterator operator++(int dummy) {
            iterator old_it(*this);
            if (this->bit_id == k_bitpage_size - 1) {
                this->bit_id = 0;
                this->page_id++;
            } else {
                this->bit_id++;
            }
            return old_it;
        }

        iterator &operator--() {
            if (this->bit_id == 0) {
                this->bit_id = k_bitpage_size - 1;
                this->page_id--;
            } else {
                this->bit_id--;
            }
            return *this;
        }

        iterator operator--(int dummy) {
            iterator old_it(*this);
            if (this->bit_id == 0) {
                this->bit_id = k_bitpage_size - 1;
                this->page_id--;
            } else {
                this->bit_id--;
            }
            return old_it;
        }


        // COMPARISON OPERATORS
        const bool operator<(const iterator &rhs) {
            return this->_index() < rhs._index();
        }

        const bool operator>(const iterator &rhs) {
            return this->_index() > rhs._index();
        }

        const bool operator<=(const iterator &rhs) {
            return this->_index() <= rhs._index();
        }

        const bool operator>=(const iterator &rhs) {
            return this->_index() >= rhs._index();
        }

        const bool operator==(const iterator &rhs) {
            return this->_index() == rhs._index();
        }

        const bool operator!=(const iterator &rhs) {
            return this->_index() != rhs._index();
        }


        // ARITHMETIC OPERATORS
        friend const iterator operator+(const iterator &lhs, const int rhs) {
            return iterator(lhs.vec, lhs._index() + rhs);
        }

        friend const iterator operator+(const int lhs, const iterator &rhs) {
            return iterator(rhs.vec, rhs._index() + lhs);
        }

        friend const iterator operator-(const iterator &lhs, const int rhs) {
            return iterator(lhs.vec, lhs._index() - rhs);
        }

        friend const iterator operator-(const int lhs, const iterator &rhs) {
            return iterator(rhs.vec, rhs._index() + lhs);
        }

        friend const int operator-(const iterator &lhs, const iterator &rhs) {
            return lhs._index() - rhs._index();
        }


        // COMPOUND ASSIGNMENT OPERATORS
        const iterator &operator+=(const int rhs) {
            return *this = *this + rhs;
        }

        const iterator &operator-=(const int rhs) {
            return *this = *this - rhs;
        }
    };

    // CONSTRUCTOR
    Vector(size_t size = 0, size_t capacity = 0)
        : pages(_required_page_count(size), _required_page_count(capacity))
        , cap(capacity)
        , len(size)
    {
        assert(capacity == 0 || capacity >= size);
        //
    }

    // Default copy/move constructors should be fine.
    // Default destructor should be fine.

    // ACCESSORS
    uint32_t *data() {
        return this->pages.data();
    }

    const uint32_t *data() const {
        return this->pages.data();
    }

    size_t capacity() {
        return this->cap;
    }

    const size_t capacity() const {
        return this->cap;
    }

    size_t size() {
        return this->len;
    }

    const size_t size() const {
        return this->len;
    }

    iterator begin() {
        return iterator(this);
    }

    iterator end() {
        return iterator(this, size());
    }

    _write_helper at(size_t index) {
        assert(index < size());
        return (*this)[index];
    }

    const bool at(size_t index) const {
        assert(index < size());
        return (*this)[index];
    }


    // MODIFIERS
    void resize(size_t size) {
        reserve(size);

        if (size > this->len) {
            for (size_t page_id = _required_page_count(this->len);
                page_id < _required_page_count(size);
                page_id++)
            {
                this->pages.at(page_id) = 0;
            }
            this->pages.at(_index_to_page_id(this->len)) &=
                _bit_mask(0, _index_to_bit_id(this->len) + 1);
        }

        this->len = size;
    }

    void clear() {
        resize(0);
    }

    void reserve(size_t new_cap) {
        if (new_cap > this->cap) {
            this->pages.resize(_required_page_count(new_cap));
            this->cap = new_cap;
        }
    }

    void shrink_to_fit() {
        assert(this->len <= this->cap);
        if (this->len < this->cap) {
            this->pages.shrink_to_fit();
            this->cap = this->len;
        }
    }

    void push_back(const bool val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->at(this->len++) = val;
    }

    void insert(iterator pos, const bool val) {
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);

        iterator shift_page_tail_it = end() - 1;
        while (shift_page_tail_it >= pos) {
            if (shift_page_tail_it.bit_id == k_bitpage_size - 1) {
                // Handle bit overflow
                iterator next_page_head_it = shift_page_tail_it + 1;
                *next_page_head_it = *shift_page_tail_it;
            }

            BitPage shift_page = this->pages.at(shift_page_tail_it.page_id);
            size_t shift_front_bit_id =
                shift_page_tail_it.page_id == pos.page_id ?
                    pos.bit_id : 0;

            BitPage shift_mask = _bit_mask(shift_front_bit_id, k_bitpage_size);
            this->pages.at(shift_page_tail_it.page_id) =
                (shift_page & ~shift_mask) | ((shift_page & shift_mask) >> 1);

            shift_page_tail_it -= shift_page_tail_it.bit_id + 1;
        }
        *pos = val;
        this->len++;
    }

    void erase(iterator first, iterator last) {
        assert(first <= last);
        assert(first >= begin() && first <= end());
        assert(last >= begin() && last <= end());

        size_t old_last_page_id = this->_last_page_id();
        iterator move_src_it = last;
        iterator move_dst_it = first;
        while (move_src_it < end()) {
            // printf("original page: %u\n", this->pages.at(0));
            size_t src_to_page_end =
                ((size_t) move_src_it.page_id == old_last_page_id ?
                    end().bit_id : k_bitpage_size) - move_src_it.bit_id;
            size_t dst_to_page_end =
                ((size_t) move_dst_it.page_id == old_last_page_id ?
                    end().bit_id : k_bitpage_size) - move_dst_it.bit_id;

            size_t shift_count = dst_to_page_end > src_to_page_end ?
                src_to_page_end : dst_to_page_end;
            assert(shift_count != 0);

            // printf("src %lu: %lu\n", move_src_it.page_id, move_src_it.bit_id);
            // printf("dst %lu: %lu\n", move_dst_it.page_id, move_dst_it.bit_id);
            BitPage dst_mask = ~_bit_mask(move_dst_it.bit_id,
                move_dst_it.bit_id + shift_count);
            BitPage src_mask =
                _bit_mask(move_src_it.bit_id, move_src_it.bit_id + shift_count);
            size_t src_to_dst_offset = move_src_it - move_dst_it;

            this->pages.at(move_dst_it.page_id) =
                (this->pages.at(move_dst_it.page_id) & dst_mask) |
                ((this->pages.at(move_src_it.page_id) & src_mask)
                    << src_to_dst_offset);
            this->pages.at(move_src_it.page_id) &= ~src_mask | dst_mask;

            // printf("shift_count: %u\n", shift_count);
            // printf("src to end: %u\n", src_to_page_end);
            // printf("dst to end: %u\n", dst_to_page_end);
            // printf("offset: %u\n", src_to_dst_offset);
            // printf("dst_mask: %u\n", dst_mask);
            // printf("src_mask: %u\n", src_mask);
            // printf("new dst: %u\n", this->pages.at(move_dst_it.page_id) & dst_mask);
            // printf("new src: %u\n", ((this->pages.at(move_src_it.page_id) & src_mask)
            //         << src_to_dst_offset));

            move_src_it += shift_count;
            move_dst_it += shift_count;
        }

        this->len -= last - first;
    }

    void erase(iterator pos) {
        erase(pos, pos + 1);
    }


    // OPERATORS
    _write_helper operator[](size_t index) {
        return _write_helper(this, index);
    }

    const bool operator[](size_t index) const {
        const iterator result_it(this, index);
        return *result_it;
    }

    // Default copy/move assignment should be fine.
};
