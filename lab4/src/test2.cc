
#include <stdio.h>

#include "vector.hh"

int make2() {
    return 2;
}

int main(int argc, char **argv) {
    printf("\nRunning test2\n");

    Vector<int*> test_v0;
    test_v0.push_back(nullptr);

    Vector<float*> test_v1;
    test_v1.push_back(nullptr);

    /*
    tests for the insert function

    int arg = 3;

    test_v0.insert(test_v0.end(), 10);
    test_v0.insert(test_v0.begin(), make2());
    test_v0.insert(test_v0.begin() + 1, arg);

    printf("contents of test_v0 after inserts: ");
    for (Vector<int>::iterator i = test_v0.begin(); i != test_v0.end(); i++) {
        printf("%d ", *i);
    }
    printf("\n");

    test_v0.erase(test_v0.begin(), test_v0.begin() + 2);

    printf("contents of test_v0 after erase: ");
    for (Vector<int>::iterator i = test_v0.begin(); i != test_v0.end(); i++) {
        printf("%d ", *i);
    }
    printf("\n");

    output:
    Running test2
    MOVE INSERT
    MOVE INSERT
    COPY INSERT
    contents of test_v0 after inserts: 2 3 1 10 
    contents of test_v0 after erase: 1 10 
    DONE

    */


    printf("DONE\n\n");

    return 0;
}
