
#include <stdlib.h>
#include <cstring>
#include <cassert>
#include <utility>

template<typename T>
class Vector {
private:
    T *vals;
    size_t cap;
    size_t len;

    void copy_helper(const Vector<T> &vec) {
        this->vals = (T*) malloc(vec.capacity() * sizeof (T));
        std::memcpy(this->vals, vec.data(), vec.size() * sizeof (T));
        this->cap = vec.capacity();
        this->len = vec.size();
    }

    void move_helper(Vector<T> &&vec) {
        if (this != &vec) {
            this->cap = vec.capacity();
            this->len = vec.size();
            if (this->vals) {
                delete[] this->vals;
                this->vals = nullptr;
            }
            this->vals = vec.data();
            vec.vals = nullptr;
        }
    }

    void cleanup_helper() {
        if (this->vals) {
            delete[] this->vals;
            this->vals = nullptr;
        }
        this->cap = 0;
        this->len = 0;
    }


public:
    typedef T* iterator;

    // CONSTRUCTOR
    Vector(size_t size = 0, size_t capacity = 0)
        : vals((T*) malloc((capacity == 0 ? size : capacity) * sizeof (T)))
        , cap(capacity == 0 ? size : capacity)
        , len(size)
    {
        assert(capacity == 0 || capacity >= size);
        //
    }

    Vector(const Vector<T> &vec) : vals(nullptr), cap(0), len(0) {
        copy_helper(vec);
    }

    Vector(Vector<T> &&vec) : vals(nullptr), cap(0), len(0) {
        move_helper(std::forward< Vector<T> >(vec));
    }

    ~Vector() {
        cleanup_helper();
    }


    // ACCESSORS
    T *data() {
        return this->vals;
    }

    const T *data() const {
        return this->vals;
    }

    size_t capacity() {
        return this->cap;
    }

    const size_t capacity() const {
        return this->cap;
    }

    size_t size() {
        return this->len;
    }

    const size_t size() const {
        return this->len;
    }

    iterator begin() {
        return this->vals;
    }

    iterator end() {
        return this->vals + this->len;
    }


    // MODIFIERS
    void resize(size_t size) {
        if (size > this->cap) {
            this->vals = (T*) realloc(this->vals, size * sizeof (T));
            this->cap = size;
        }

        if (size < this->len) {
            for (size_t i = size; i < this->len; i++) {
                this->vals[i] = T();
            }
        }

        this->len = size;
    }

    void clear() {
        resize(0);
    }

    void reserve(size_t new_cap) {
        if (new_cap > this->cap) {
            this->vals = (T*) realloc(this->vals, new_cap * sizeof (T));
            this->cap = new_cap;
        }
    }

    void shrink_to_fit() {
        assert(this->len <= this->cap);
        if (this->len < this->cap) {
            this->vals = (T*) realloc(this->vals, this->len * sizeof (T));
            this->cap = this->len;
        }
    }

    void push_back(const T &val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = val;
    }

    void push_back(T &&val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = std::forward<T>(val);
    }

    void insert(iterator pos, const T &val) {
        // printf("COPY INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);
        memmove(pos + 1, pos, (end() - pos) * sizeof (T));
        *pos = val;
        this->len++;
    }

    void insert(iterator pos, T &&val) {
        // printf("MOVE INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);
        memmove(pos + 1, pos, (end() - pos) * sizeof (T));
        *pos = std::forward<T>(val);
        this->len++;
    }

    void erase(iterator first, iterator last) {
        assert(first >= begin() && first <= end());
        assert(last >= begin() && last <= end());
        memmove(first, last, (end() - last) * sizeof (T));
        this->len -= last - first;
    }

    void erase(iterator pos) {
        erase(pos, pos + 1);
    }

    void null_set_data() {
        this->vals = nullptr;
    }


    // OPERATORS
    T &operator[](size_t index) {
        return this->vals[index];
    }

    const T &operator[](size_t index) const {
        return this->vals[index];
    }

    Vector &operator=(const Vector<T> &vec) {
        cleanup_helper();
        copy_helper(vec);
        return *this;
    }

    Vector &operator=(Vector<T> &&vec) {
        cleanup_helper();
        move_helper(std::forward< Vector<T> >(vec));
        return *this;
    }
};
