
class Cell;

class Entity
{
  private:
    Cell &cell;

    // Disable default constructor (DO NOT DEFINE)
    Entity();

  public:
    Entity(Cell &cell); // TODO: Implement in entities.cpp
    ~Entity();  // TODO: Fix this declaration to allow polymorphism and
                // implement in entities.cpp

    virtual void act() = 0;
    virtual char toChar() = 0;
};

/*
TODO: Declare (and define in entities.cpp (LAB 6)) child classes for all of the
following entity types. For Lab 5, you don't have to define any behavior. In
fact, all these child classes can have blank act() implemenations, toChar()
implemenations and nothing else. Just make sure that the cell reference is setup
correctly and that the cell being referenced by the entity has the Entity
pointer cached correctly.

Structure (symbols):
# = wall
+ = door
! = hidden door (will be drawn like a wall in the game)
. = location where players will spawn

Items (symbols and a-z):
$ = treasure
p = potion
w = weapon

Foul Creatures (A-Z):
B = bat
G = grue
S = snake
T = troll
*/
