
#include <stdlib.h>

#include "display.hpp"
#include "map.hpp"

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        printf("usage: %s <path_to_map_file>\n", argv[0]);
        exit(1);
    }

    Map map;
    // TODO: Load map using Map::loadFromFile(const char *filename);

    Display display;

    while (true)
    {
        display.drawMap(map);

        if (getch() == 27)
        {
            break;
        }
    }

    return 0;
}
