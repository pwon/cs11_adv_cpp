#include "map.hpp"

Cell::Cell(const Map &map) : map(map), occupier(nullptr) {}
Cell::~Cell() {}

Map::Map(unsigned int width, unsigned int height)
    : width(width), height(height), cells()
{
    // Nothing goes here
}

const unsigned int Map::getWidth() const
{
    return this->width;
}

const unsigned int Map::getHeight() const
{
    return this->height;
}
