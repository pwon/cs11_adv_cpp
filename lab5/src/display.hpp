
#include <ncurses.h>
// sudo apt-get install libncurses5-dev libncursesw5-dev

class Map;

class Display
{
  private:
    // The window where the map is drawn
    WINDOW *mapWindow;

    // The window where game messages are reported
    WINDOW *msgWindow;

    // The window where other information will be displayed
    WINDOW *infoWindow;

  public:
    Display();
    ~Display();

    void drawMap(const Map &map);

  public:
    // The number of rows that will be taken up by the message window
    static const int MsgWinRows = 5;

    // The number of columns that will be taken up by the information window
    static const int InfoWinCols = 15;
};
