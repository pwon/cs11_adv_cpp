#include "display.hpp"

#include <iostream>

#include "map.hpp"

using namespace std;

/*!
 * This function initializes ncurses and sets up three "windows" in the console,
 * one for the map, another for game messages, and the final one for other
 * information that we might want to display.
 */
Display::Display()
{
    initscr();

    // Have curses return each character as it is typed.  This means that curses
    // won't do backspace and stuff like that.
    cbreak();
    noecho();

    raw();
    keypad(stdscr, TRUE);

    // Make the cursor invisible
    curs_set(0);

    mapWindow = newwin(LINES - MsgWinRows, COLS - InfoWinCols, 0, 0);
    infoWindow = newwin(LINES - MsgWinRows, InfoWinCols, 0, COLS - InfoWinCols);
    msgWindow = newwin(MsgWinRows, COLS, LINES - MsgWinRows, 0);

    refresh();
}

// Shuts down and cleans up ncurses.
Display::~Display()
{
    // Delete each of the windows
    delwin(mapWindow);
    delwin(infoWindow);
    delwin(msgWindow);

    // Shut down ncurses.=
    endwin();
}

void Display::drawMap(const Map &map)
{
    for (unsigned int x = 0; x < map.getWidth(); x++)
    {
        for (unsigned int y = 0; y < map.getHeight(); y++)
        {
            // TODO: Change to draw based on map contents
            wmove(this->mapWindow, y, x);
            waddch(this->mapWindow, '_');
        }
    }

    // TODO: LAB 6: Implement info window and game messages window.

    wrefresh(this->mapWindow);
}