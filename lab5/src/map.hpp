
#include <vector>

class Map;
class Entity;

class Cell
{
  private:
    const Map &map;
    Entity *occupier;

    // Disable default constructor (DO NOT DEFINE)
    Cell();

  public:
    Cell(const Map &map);
    ~Cell(); 
};

class Map
{
  private:
    static const unsigned int k_default_width = 80;
    static const unsigned int k_default_height = 24;

    unsigned int width;
    unsigned int height;

    std::vector<Cell> cells;

  public:
    Map(unsigned int width = k_default_width,
        unsigned int height = k_default_height);
    ~Map() = default;

    const unsigned int getWidth() const;
    const unsigned int getHeight() const;

    Cell &getCell(int x, int y) const;          // TODO: implement in map.cpp
    void setCell(int x, int y, Entity *entity); // TODO: implement in map.cpp

    void loadFromFile(const char *filename);    // TODO: implement in map.cpp
};
