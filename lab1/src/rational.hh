
#include <iostream>

class Rational {
private:
    int n;
    int d;

    int findGCD() const;
    void validate() const;
    void simplify();

public:
    Rational();
    Rational(int num);
    Rational(int num, int denom);
    ~Rational();

    const int num();
    const int denom();

    const int num() const;
    const int denom() const;

    const Rational reciprocal();

    const Rational &operator=(const Rational &rhs);

    friend const Rational operator+(const Rational &lhs, const Rational &rhs);
    friend const Rational operator-(const Rational &lhs, const Rational &rhs);
    friend const Rational operator*(const Rational &lhs, const Rational &rhs);
    friend const Rational operator/(const Rational &lhs, const Rational &rhs);

    const Rational &operator++();
    const Rational &operator--();
    const Rational operator++(int dummy);
    const Rational operator--(int dummy);

    const Rational &operator+=(const Rational &rhs);
    const Rational &operator-=(const Rational &rhs);
    const Rational &operator*=(const Rational &rhs);
    const Rational &operator/=(const Rational &rhs);

    const Rational operator-();

    const bool operator<(const Rational &rhs);
    const bool operator>(const Rational &rhs);
    const bool operator<=(const Rational &rhs);
    const bool operator>=(const Rational &rhs);
    const bool operator==(const Rational &rhs);
    const bool operator!=(const Rational &rhs);

    operator float();
    operator double();

    friend std::ostream &operator<<(std::ostream &os, const Rational &rhs);
};
