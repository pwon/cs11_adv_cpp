#include "rational.hh"

#include <cmath>
#include <stdexcept>
#include <cassert>

using namespace std;

Rational::Rational() : n(0), d(1) {}

Rational::Rational(int num) : n(num), d(1) {}

Rational::Rational(int num, int denom) : n(num), d(denom) {
    if (this->denom() == 0) {
        throw invalid_argument("divide by zero");
    }
    this->simplify();
}

Rational::~Rational() {}

int Rational::findGCD() const {
    int temp_0 = abs(this->n);
    int temp_1 = abs(this->d);
    int temp_2;
    while (temp_0 != 0) {
        temp_2 = temp_0;
        temp_0 = temp_1 % temp_0;
        temp_1 = temp_2;
    }
    return temp_1;
}

void Rational::validate() const {
    assert(this->d > 0);
    if (this-> n == 0) {
        assert(this->d == 1);
    }
    assert(this->findGCD() == 1);
}

void Rational::simplify() {
    if (this->n == 0) {
        this->d = 1;
        return;
    }

    if (this->d < 0) {
        this->n *= -1;
        this->d *= -1;
    }

    int gcd = findGCD();
    this->n /= gcd;
    this->d /= gcd;
}


const int Rational::num() {
    return this->n;
}

const int Rational::denom() {
    return this->d;
}

const int Rational::num() const {
    return this->n;
}

const int Rational::denom() const {
    return this->d;
}


const Rational Rational::reciprocal() {
    if (this->num() == 0) {
        throw invalid_argument("divide by zero");
    }
    Rational result(this->denom(), this->num());
    result.simplify();
    return result;
}


const Rational &Rational::operator=(const Rational &rhs) {
    rhs.validate();
    this->n = rhs.num();
    this->d = rhs.denom();
    return *this;
}


const Rational operator+(const Rational &lhs, const Rational &rhs) {
    lhs.validate();
    rhs.validate();
    Rational result(lhs.num() * rhs.denom() + lhs.denom() * rhs.num(),
                    lhs.denom() * rhs.denom());
    result.simplify();
    return result;
}

const Rational operator-(const Rational &lhs, const Rational &rhs) {
    lhs.validate();
    rhs.validate();
    Rational result(lhs.num() * rhs.denom() - lhs.denom() * rhs.num(),
                    lhs.denom() * rhs.denom());
    result.simplify();
    return result;
}

const Rational operator*(const Rational &lhs, const Rational &rhs) {
    lhs.validate();
    rhs.validate();
    Rational result(lhs.num() * rhs.num(), lhs.denom() * rhs.denom());
    result.simplify();
    return result;
}

const Rational operator/(const Rational &lhs, const Rational &rhs) {
    lhs.validate();
    rhs.validate();
    if (rhs.num() == 0) {
        throw invalid_argument("divide by zero");
    }
    Rational result(lhs.num() * rhs.denom(), lhs.denom() * rhs.num());
    result.simplify();
    return result;
}


const Rational &Rational::operator++() {
    this->validate();
    this->n += this->d;
    this->simplify();
    return *this;
}

const Rational &Rational::operator--() {
    this->validate();
    this->n -= this->d;
    this->simplify();
    return *this;
}

const Rational Rational::operator++(int dummy) {
    this->validate();
    Rational result(*this);
    this->n += this->d;
    this->simplify();
    return result;
}

const Rational Rational::operator--(int dummy) {
    this->validate();
    Rational result(*this);
    this->n -= this->d;
    this->simplify();
    return result;
}


const Rational &Rational::operator+=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return *this = *this + rhs;
}

const Rational &Rational::operator-=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return *this = *this - rhs;
}

const Rational &Rational::operator*=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return *this = *this * rhs;
}

const Rational &Rational::operator/=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return *this = *this / rhs;
}


const Rational Rational::operator-() {
    this->validate();
    Rational result(*this);
    return result *= -1;
}


const bool Rational::operator<(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return this->num() * rhs.denom() < this->denom() * rhs.num();
}

const bool Rational::operator>(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return this->num() * rhs.denom() > this->denom() * rhs.num();
}

const bool Rational::operator<=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return this->num() * rhs.denom() <= this->denom() * rhs.num();
}

const bool Rational::operator>=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return this->num() * rhs.denom() >= this->denom() * rhs.num();
}

const bool Rational::operator==(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return this->num() * rhs.denom() == this->denom() * rhs.num();
}

const bool Rational::operator!=(const Rational &rhs) {
    this->validate();
    rhs.validate();
    return this->num() * rhs.denom() != this->denom() * rhs.num();
}


Rational::operator float() {
    this->validate();
    return this->n / (float) this->d;
}

Rational::operator double() {
    this->validate();
    return this->n / (double) this->d;
}


ostream &operator<<(ostream &os, const Rational &rhs) {
    rhs.validate();
    if (rhs.denom() == 1) {
        os << rhs.num();
    } else {
        os << rhs.num() << "/" << rhs.denom();
    }
    return os;
}

