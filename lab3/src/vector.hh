
#include <stdlib.h>
#include <cstring>
#include <cassert>
#include <utility>

template<typename T>
class Vector {
private:
    T *vals;
    size_t cap;
    size_t len;

    void copy_helper(const Vector<T> &vec) {
        this->vals = (T*) malloc(vec.capacity() * sizeof (T));
        std::memcpy(this->vals, vec.data(), vec.size() * sizeof (T));
        this->cap = vec.capacity();
        this->len = vec.size();
    }

    void move_helper(Vector<T> &&vec) {
        if (this != &vec) {
            this->cap = vec.capacity();
            this->len = vec.size();
            if (this->vals) {
                delete[] this->vals;
                this->vals = nullptr;
            }
            this->vals = vec.data();
            vec.vals = nullptr;
        }
    }

    void cleanup_helper() {
        if (this->vals) {
            delete[] this->vals;
            this->vals = nullptr;
        }
        this->cap = 0;
        this->len = 0;
    }


public:
    typedef T* iterator;

    // CONSTRUCTOR
    Vector(size_t size = 0, size_t capacity = 0)
        : vals((T*) malloc((capacity == 0 ? size : capacity) * sizeof (T)))
        , cap(capacity == 0 ? size : capacity)
        , len(size)
    {
        assert(capacity == 0 || capacity >= size);
        //
    }

    Vector(const Vector<T> &vec) : vals(nullptr), cap(0), len(0) {
        copy_helper(vec);
    }

    Vector(Vector<T> &&vec) : vals(nullptr), cap(0), len(0) {
        move_helper(std::forward< Vector<T> >(vec));
    }

    ~Vector() {
        cleanup_helper();
    }


    // ACCESSORS
    T *data() {
        return this->vals;
    }

    const T *data() const {
        return const_cast<const T*>(this->vals);
    }

    size_t capacity() {
        return this->cap;
    }

    const size_t capacity() const {
        return this->cap;
    }

    size_t size() {
        return this->len;
    }

    const size_t size() const {
        return this->len;
    }

    iterator begin() {
        return this->vals;
    }

    iterator end() {
        return this->vals + this->len;
    }


    // MODIFIERS
    void resize(size_t size) {
        if (size > this->cap) {
            this->vals = (T*) realloc(this->vals, size * sizeof (T));
            this->cap = size;
        }

        if (size < this->len) {
            for (size_t i = size; i < this->len; i++) {
                this->vals[i] = T();
            }
        }

        this->len = size;
    }

    void clear() {
        resize(0);
    }

    void reserve(size_t new_cap) {
        if (new_cap > this->cap) {
            this->vals = (T*) realloc(this->vals, new_cap * sizeof (T));
            this->cap = new_cap;
        }
    }

    void shrink_to_fit() {
        assert(this->len <= this->cap);
        if (this->len < this->cap) {
            this->vals = (T*) realloc(this->vals, this->len * sizeof (T));
            this->cap = this->len;
        }
    }

    void push_back(const T &val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = val;
    }

    void push_back(T &&val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = std::forward<T>(val);
    }

    void insert(iterator pos, const T &val) {
        // printf("COPY INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);
        memmove(pos + 1, pos, (end() - pos) * sizeof (T));
        *pos = val;
        this->len++;
    }

    void insert(iterator pos, T &&val) {
        // printf("MOVE INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);
        memmove(pos + 1, pos, (end() - pos) * sizeof (T));
        *pos = std::forward<T>(val);
        this->len++;
    }

    void erase(iterator first, iterator last) {
        assert(first >= begin() && first <= end());
        assert(last >= begin() && last <= end());
        memmove(first, last, (end() - last) * sizeof (T));
        this->len -= last - first;
    }

    void erase(iterator pos) {
        erase(pos, pos + 1);
    }

    void null_set_data() {
        this->vals = nullptr;
    }


    // OPERATORS
    T &operator[](size_t index) {
        return this->vals[index];
    }

    const T &operator[](size_t index) const {
        return this->vals[index];
    }

    Vector &operator=(const Vector<T> &vec) {
        cleanup_helper();
        copy_helper(vec);
        return *this;
    }

    Vector &operator=(Vector<T> &&vec) {
        cleanup_helper();
        move_helper(std::forward< Vector<T> >(vec));
        return *this;
    }
};


template<>
class Vector<void*> {
private:
    void **vals;
    size_t cap;
    size_t len;

    void copy_helper(const Vector<void*> &vec) {
        this->vals = (void**) malloc(vec.capacity() * sizeof (void*));
        std::memcpy(this->vals, vec.data(), vec.size() * sizeof (void*));
        this->cap = vec.capacity();
        this->len = vec.size();
    }

    void move_helper(Vector<void*> &&vec) {
        if (this != &vec) {
            this->cap = vec.capacity();
            this->len = vec.size();
            if (this->vals) {
                delete[] this->vals;
                this->vals = nullptr;
            }
            this->vals = vec.data();
            vec.vals = nullptr;
        }
    }

    void cleanup_helper() {
        if (this->vals) {
            delete[] this->vals;
            this->vals = nullptr;
        }
        this->cap = 0;
        this->len = 0;
    }


public:
    typedef void** iterator;

    // CONSTRUCTOR (private to disable direct construction of Vector)
    Vector(size_t size = 0, size_t capacity = 0)
        : vals((void**) malloc((capacity == 0 ? size : capacity)
            * sizeof (void*)))
        , cap(capacity == 0 ? size : capacity)
        , len(size)
    {
        assert(capacity == 0 || capacity >= size);
        //
    }

    Vector(const Vector<void*> &vec) : vals(nullptr), cap(0), len(0) {
        copy_helper(vec);
    }

    Vector(Vector<void*> &&vec) : vals(nullptr), cap(0), len(0) {
        move_helper(std::forward< Vector<void*> >(vec));
    }

    ~Vector() {
        cleanup_helper();
    }


    // ACCESSORS
    void **data() {
        return this->vals;
    }

    const void **data() const {
        return const_cast<const void**>(this->vals);
    }

    size_t capacity() {
        return this->cap;
    }

    const size_t capacity() const {
        return this->cap;
    }

    size_t size() {
        return this->len;
    }

    const size_t size() const {
        return this->len;
    }

    iterator begin() {
        return this->vals;
    }

    iterator end() {
        return this->vals + this->len;
    }


    // MODIFIERS
    void resize(size_t size) {
        if (size > this->cap) {
            this->vals = (void**) realloc(this->vals, size * sizeof (void*));
            this->cap = size;
        }

        if (size < this->len) {
            for (size_t i = size; i < this->len; i++) {
                this->vals[i] = nullptr;
            }
        }

        this->len = size;
    }

    void clear() {
        resize(0);
    }

    void reserve(size_t new_cap) {
        if (new_cap > this->cap) {
            this->vals = (void**) realloc(this->vals, new_cap * sizeof (void*));
            this->cap = new_cap;
        }
    }

    void shrink_to_fit() {
        assert(this->len <= this->cap);
        if (this->len < this->cap) {
            this->vals =
                (void**) realloc(this->vals, this->len * sizeof (void*));
            this->cap = this->len;
        }
    }

    void push_back(const void* &val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = const_cast<void*>(val);
    }

    void push_back(void* &&val) {
        assert(this->len <= this->cap);

        if (this->cap == 0) {
            reserve(1);
        }

        if (this->len == this->cap) {
            reserve(this->cap * 2);
        }

        this->vals[this->len++] = std::forward<void*>(val);
    }

    void insert(iterator pos, const void* &val) {
        // printf("COPY INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);
        memmove(pos + 1, pos, (end() - pos) * sizeof (void*));
        *pos = const_cast<void*>(val);
        this->len++;
    }

    void insert(iterator pos, void* &&val) {
        // printf("MOVE INSERT\n"); <- uncomment for testing (look at test2.cc)
        assert(pos >= begin() && pos <= end());
        reserve(this->len + 1);
        memmove(pos + 1, pos, (end() - pos) * sizeof (void*));
        *pos = std::forward<void*>(val);
        this->len++;
    }

    void erase(iterator first, iterator last) {
        assert(first >= begin() && first <= end());
        assert(last >= begin() && last <= end());
        memmove(first, last, (end() - last) * sizeof (void*));
        this->len -= last - first;
    }

    void erase(iterator pos) {
        erase(pos, pos + 1);
    }

    void null_set_data() {
        this->vals = nullptr;
    }


    // OPERATORS
    void* &operator[](size_t index) {
        return this->vals[index];
    }

    const void* &operator[](size_t index) const {
        return const_cast<const void**>(this->vals)[index];
    }

    Vector &operator=(const Vector<void*> &vec) {
        cleanup_helper();
        copy_helper(vec);
        return *this;
    }

    Vector &operator=(Vector<void*> &&vec) {
        cleanup_helper();
        move_helper(std::forward< Vector<void*> >(vec));
        return *this;
    }
};

template<typename T>
class Vector<T*> : private Vector<void*> {
private:
    typedef Vector<void*> Base;

public:
    typedef T** iterator;

    // CONSTRUCTOR
    Vector(size_t size = 0, size_t capacity = 0) : Base(size, capacity) {}
    Vector(const Vector<T*> &vec)
        : Base(reinterpret_cast<const Base&>(vec)) {}
    Vector(Vector<T*> &&vec)
        : Base(std::forward<Base>(reinterpret_cast<Base&&>(vec))) {}
    ~Vector() {}

    // ACCESSORS
    T **data() { return reinterpret_cast<T**>(Base::data()); }
    const T **data() const { return reinterpret_cast<const T**>(Base::data()); }
    size_t capacity() { return Base::capacity(); }
    const size_t capacity() const { return Base::capacity(); }
    size_t size() { return Base::size(); }
    const size_t size() const { return Base::size(); }
    iterator begin() { return reinterpret_cast<iterator>(Base::begin()); }
    iterator end() { return reinterpret_cast<iterator>(Base::end()); }

    // MODIFIERS
    void resize(size_t size) { Base::resize(size); }
    void clear() { Base::clear(); }
    void reserve(size_t new_cap) { Base::reserve(new_cap); }
    void shrink_to_fit() { Base::shrink_to_fit(); }
    void push_back(const T* &val) {
        Base::push_back(reinterpret_cast<const void* &>(val));
    }
    void push_back(T* &&val) {
        Base::push_back(std::forward<void*>(reinterpret_cast<void* &&>(val)));
    }
    void insert(iterator pos, const T* &val) {
        Base::insert(pos, reinterpret_cast<const void* &>(val));
    }
    void insert(iterator pos, T* &&val) {
        Base::insert(pos, std::forward<void*>(reinterpret_cast<void* &&>(val)));
    }
    void erase(iterator first, iterator last) {
        Base::erase(reinterpret_cast<Base::iterator>(first),
            reinterpret_cast<Base::iterator>(last));
    }
    void erase(iterator pos) {
        Base::erase(reinterpret_cast<Base::iterator>(pos));
    }
    void null_set_data() { Base::null_set_data(); }

    // OPERATORS
    T* &operator[](size_t index) {
        return reinterpret_cast<T* &>(Base::operator[](index));
    }
    const T* &operator[](size_t index) const {
        return reinterpret_cast<const T* &>(Base::operator[](index));
    }
    Vector &operator=(const Vector<T*> &vec) {
        return reinterpret_cast< Vector<T*>& >(
            Base::operator=(reinterpret_cast<const Base&>(vec)));
    }
    Vector &operator=(Vector<T*> &&vec) {
        return reinterpret_cast< Vector<T*>& >(
            Base::operator=(std::forward<Base>(reinterpret_cast<Base&&>(vec))));
    }
};
