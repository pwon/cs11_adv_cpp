
#include <stdio.h>

#include "vector.hh"

int main(int argc, char **argv) {
    printf("\nRunning test3\n");

    Vector<int*> test_v0;
    test_v0.push_back(nullptr);

    Vector<float*> test_v1;
    test_v1.push_back(nullptr);

    Vector<double*> test_v2;
    test_v2.push_back(nullptr);

    printf("DONE\n\n");

    return 0;
}
